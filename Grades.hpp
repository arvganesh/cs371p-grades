// ----------
// Grades.hpp
// ----------

#ifndef Grades_hpp
#define Grades_hpp

// --------
// includes
// --------

#include <algorithm> // count, min_element, transform
#include <cassert>   // assert
#include <map>       // map
#include <string>    // string
#include <vector>    // vector

using namespace std;

static const vector<vector<int>> grade_table = {
    {5, 5, 4, 4, 4, 4, 4, 4, 3, 3, 3}, /* projects */
    {11, 11, 10, 10, 10, 9, 9, 8, 8, 8, 7}, /* exercises */
    {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8}, /* blogs */
    {13, 13, 12, 12, 11, 11, 10, 10, 9, 9, 8}, /* papers */
    {39, 38, 37, 35, 34, 32, 31, 29, 28, 27, 25} /* quizzes */
};

static const vector<string> letter_grades = {"A", "A-", "B+", "B", "B-", "C+", "C", "C-", "D+", "D", "D-"};

// -----------
// grades_eval
// -----------
int get_category_score(const vector<int>& v_scores, bool should_replace_ones) {
    int one_count = count(v_scores.cbegin(), v_scores.cend(), 1);
    int two_count = count(v_scores.cbegin(), v_scores.cend(), 2);
    int three_count = count(v_scores.cbegin(), v_scores.cend(), 3);
    int result = (should_replace_ones ? min(one_count, three_count / 2) : 0) + two_count + three_count;
    return result;
}

int get_letter_grade_index(int score, vector<int> grade_cutoffs) {
    assert(score >= 0);
    for (int i = 0; i < grade_cutoffs.size(); ++i) {
        if (score >= grade_cutoffs[i]) return i;
    }

    // should never get here.
    assert(0);
    return -1;
}

string grades_eval (const vector<vector<int>>& v_v_scores) {
    assert(v_v_scores.size() == 5);
    vector<bool> should_replace_ones = {
        false, /* projects */
        true, /* exercises */
        false, /* blogs */
        true, /* papers */
        true /* quizzes */
    };

    // compute category scores.
    vector<int> category_scores(5);
    transform(v_v_scores.cbegin(), v_v_scores.cend(), should_replace_ones.cbegin(), category_scores.begin(), get_category_score);

    // compute grades for each category, store result in "category_scores".
    transform(category_scores.cbegin(), category_scores.cend(), grade_table.cbegin(), category_scores.begin(), get_letter_grade_index);
    
    // find index of lowest category grade.
    int lowest_category_idx = *max_element(category_scores.cbegin(), category_scores.cend());

    return letter_grades[lowest_category_idx];
}

#endif // Grades_hpp
