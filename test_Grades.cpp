// ---------------
// test_Grades.cpp
// ---------------

// --------
// includes
// --------

#include <map>    // map
#include <string> // string
#include <vector> // vector

#include "gtest/gtest.h"

#include "Grades.hpp"

using namespace std;

// ----------------
// test_grades_eval
// ----------------

TEST(grades_eval, test_0) {
    const vector<vector<int>> v_v_scores = {{0, 0, 0, 0, 0}};
    const string              letter     = grades_eval(v_v_scores);
    ASSERT_EQ(letter, "B-");
}
