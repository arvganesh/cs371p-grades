# CS371p: Object-Oriented Programming Grades Repo

* Name: Arvind Ganesh

* EID: ag77978

* GitLab ID: arvganesh

* HackerRank ID: (your HackerRank ID)

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: 3 hours

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
